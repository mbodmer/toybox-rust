*Combining Axum, Hyper, Tonic, and Tower for hybrid web/gRPC apps*

# fpcomplete: tokio tower tutorial

My notes at following the great tutorial on fpcomplete:

- [Part 1: Overview of Tower](https://www.fpcomplete.com/blog/axum-hyper-tonic-tower-part1/)
  - [1, DemoApp](https://gist.github.com/snoyberg/c6c54ed38ec8fac966e362eb212ab421)
  - [2, app_fn](https://gist.github.com/snoyberg/cb72a9cbefc608ec15e05ed70ced1a6b)
- [Part 2: Understanding Hyper, and first experiences with Axum](https://www.fpcomplete.com/blog/axum-hyper-tonic-tower-part2/)
- [Part 3: Demonstration of Tonic for a gRPC client/server](https://www.fpcomplete.com/blog/axum-hyper-tonic-tower-part3/)
- [Part 4: How to combine Axum and Tonic services into a single service](https://www.fpcomplete.com/blog/axum-hyper-tonic-tower-part4/)

## Service Trait

 > Tower Service is an abstraction of asynchronous: **Request -> I/O -> Response**

```Rust
// The service trait is parametrized on the `Request` it can handle, so Request can be anything.
// service has an associated type for the `Response` and `Error`
//
// The Service may not be immediately ready to handle a new incoming request. For example
// (coming from the docs on poll_ready), the server may currently be at capacity. You need to
// check poll_ready to find out whether the Service is ready to accept a new request. Then,
// when it's ready, you use call to initiate handling of a new Request.
//
// The handling of the request itself is also async, returning a Future, which can be awaited.
pub trait Service<Request> {
    // associated type for the response
    type Response;
    // and for an error
    type Error;

    // This is what it says in the generated docs
    type Future: Future;

    // But this more informative piece is in the actual source code
    type Future: Future<Output = Result<Self::Response, Self::Error>>;
    // the future is needed as the return type for asynchronous functions

    // implements the future, which can be polled/awaited
    fn poll_ready(
        &mut self,
        cx: &mut Context<'_>
    ) -> Poll<Result<(), Self::Error>>;

    // The handling of the request itself is also async, returning the Future
    fn call(&mut self, req: Request) -> Self::Future;
}
```

## Async Request/Response behaviour

A web application ends up having two kinds of async request/response behavior

- Inner: a service that accepts HTTP requests and returns HTTP responses
- Outer: a service that accepts the incoming network connections and returns an inner service

use std::{future::Future, task::Poll};

use crate::http;

// We're going to implement a new helper app_fn function which takes
// a closure as its argument. That closure will take in a Request
// value, and then return a Response.
pub fn app_fn<F, Ret>(f: F) -> AppFn<F>
where
    F: FnMut(crate::http::Request) -> Ret,
    Ret: Future<Output = Result<crate::http::Response, anyhow::Error>>,
{
    AppFn { f }
}

// we want to make sure it asynchronously returns the Response.
// So we'll need our calls to look something like:
pub struct AppFn<F> {
    f: F,
}

impl<F, Ret> tower::Service<http::Request> for AppFn<F>
where
    F: FnMut(http::Request) -> Ret,
    Ret: Future<Output = Result<http::Response, anyhow::Error>>,
{
    type Response = crate::http::Response;
    type Error = anyhow::Error;
    type Future = Ret;

    fn poll_ready(&mut self, _cx: &mut std::task::Context<'_>) -> Poll<Result<(), Self::Error>> {
        // always ready for a request
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: crate::http::Request) -> Self::Future {
        // call the wrapped closure
        (self.f)(req)
    }
}

use std::collections::HashMap;

use tower::{Service, ServiceExt};

use crate::http;

// running the fake server
// - Accepts a web application as an argument
// - Loops infinitely
// - Generates fake Request values
// - Prints out the Response values it gets from the application
//
// We want our fake web server to be able to handle requests concurrently.
// To do that, we'll use tokio::spawn to create new tasks for handling requests.
// Therefore, we need to be able to send the request handling to a separate task,
// which will require bounds of both Send and 'static. There are at least two
// different ways of handling this:
// - Cloning the App value in the main task and sending it to the spawned task
// - Creating the Future in the main task and sending it to the spawned task
//
pub async fn run<App>(mut app: App)
where
    App: Service<http::Request, Response = http::Response>,
    App::Error: std::fmt::Debug,
    App::Future: Send + 'static,
{
    loop {
        //  First we sleep for a bit and then generate our new fake request:
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;

        let req = crate::http::Request {
            path_and_query: "/fake/path?page=1".to_owned(),
            headers: HashMap::new(),
            body: Vec::new(),
        };

        // Next, we use the ready method (from the ServiceExt extension trait)
        // to check whether the service is ready to accept a new request:
        let app = match app.ready().await {
            Err(e) => {
                eprintln!("Service not able to accept requests: {:?}", e);
                continue;
            }
            Ok(app) => app,
        };

        // Once we know we can make another request, we get our Future,
        // spawn the task, and then wait for the Future to complete:
        let future = app.call(req);
        tokio::spawn(async move {
            match future.await {
                Ok(res) => println!("Successful response: {:?}", res),
                Err(e) => eprintln!("Error occurred: {:?}", e),
            }
        });
    }
}

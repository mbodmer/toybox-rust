//! What I found most helpful when trying to grok Hyper was implementing a simple app
//! without service_fn and make_service_fn. So let's go through that ourselves here.
//! We're going to create a simple counter app (I'm nothing if not predictable).
//! We'll need two different data types: one for the "app factory", and one for the app itself.
//! Let's start with the app itself:

use hyper::server::conn::AddrStream;
use hyper::{Body, Request, Response, Server};
use std::future::Ready;
use std::sync::atomic::AtomicUsize;
use std::sync::Arc;
use std::task::Poll;
use std::{convert::Infallible, net::SocketAddr};
use tower::Service;

struct DemoApp {
    counter: Arc<AtomicUsize>,
}

impl Service<Request<Body>> for DemoApp {
    type Response = Response<Body>;
    type Error = hyper::http::Error;
    type Future = Ready<Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, _cx: &mut std::task::Context) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    // This implementation uses the std::future::Ready struct to create a Future which is immediately ready.
    // In other words, our application doesn't perform any async actions.
    // I've set the Error associated type to hyper::http::Error. This error would be generated if, for example,
    // you provided invalid strings to the header method call, such as non-ASCII characters.
    // As we've seen multiple times, poll_ready just advertises that it's always ready to handle another request.
    fn call(&mut self, _req: Request<Body>) -> Self::Future {
        let counter = self
            .counter
            .fetch_add(1, std::sync::atomic::Ordering::SeqCst);
        let res = Response::builder()
            .status(200)
            .header("Content-Type", "text/plain; charset=utf-8")
            .body(format!("Counter is at: {counter}").into());
        std::future::ready(res)
    }
}

struct DemoAppFactory {
    counter: Arc<AtomicUsize>,
}

// We have a different parameter to Service, this time &AddrStream.
// I did initially find the naming here confusing.
// In Tower, a Service takes some Request. And with our DemoApp, the Request it takes is a Hyper Request<Body>.
// But in the case of DemoAppFactory, the Request it's taking is a &AddrStream.
// Keep in mind that a Service is really just a generalization of failable, async functions from input to output.
// The input may be a Request<Body>, or may be a &AddrStream, or something else entirely.
impl Service<&AddrStream> for DemoAppFactory {
    // the "response" here isn't an HTTP response, but a DemoApp.
    // I again find it easier to use the terms "input" and "output" to avoid the name overloading of request and response.
    type Response = DemoApp;
    type Error = Infallible;
    type Future = Ready<Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, _cx: &mut std::task::Context) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, conn: &AddrStream) -> Self::Future {
        println!("Accepting a new connection from {conn:?}");
        std::future::ready(Ok(DemoApp {
            counter: self.counter.clone(),
        }))
    }
}

// Using this level of abstraction for writing a normal web app is painful for (at least) three different reasons:

// - Managing all of these Service pieces manually is a pain
// - There's very little in the way of high level helpers, like "parse the request body as a JSON value"
// - Any kind of mistake in your types may lead to very large, non-local error messages that are difficult to diagnose

// So we'll be more than happy to move on from Hyper to Axum a bit later.

#[tokio::main]
async fn main() {
    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));

    let factory = DemoAppFactory {
        counter: Arc::new(AtomicUsize::new(0)),
    };

    let server = Server::bind(&addr).serve(factory);

    if let Err(e) = server.await {
        eprintln!("server error: {e}");
    }
}

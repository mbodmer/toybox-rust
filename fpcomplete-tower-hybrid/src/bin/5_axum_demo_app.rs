use axum::extract::Extension;
use axum::handler::get;
use axum::{AddExtensionLayer, Router};
use hyper::{HeaderMap, Server, StatusCode};
use std::net::SocketAddr;
use std::sync::atomic::AtomicUsize;
use std::sync::Arc;

#[derive(Clone, Default)]
struct AppState {
    counter: Arc<AtomicUsize>,
}

// The home function uses an extractor to get the AppState, and returns a value of type (StatusCode, HeaderMap, String)
// to represent the response. In Axum, any implementation of the appropriately named IntoResponse trait can be
// returned from handler functions.
async fn home(state: Extension<AppState>) -> (StatusCode, HeaderMap, String) {
    let counter = state
        .counter
        .fetch_add(1, std::sync::atomic::Ordering::SeqCst);
    let mut headers = HeaderMap::new();
    headers.insert("Content-Type", "text/plain; charset=utf-8".parse().unwrap());
    let body = format!("Counter is at: {counter}");
    (StatusCode::OK, headers, body)
}

#[tokio::main]
async fn main() {
    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));

    // The first thing I'd like to get out of the way is this whole AddExtensionLayer/Extension bit.
    // This is how we're managing shared state within our application.
    // It's not directly relevant to our overall analysis of Tower and Hyper, so I'll suffice with a
    // link to the docs demonstrating how this works. Interestingly, you may notice that this implementation
    // relies on middlewares, which does in fact leverage Tower, so it's not completely separate.

    // Within our main function, we're now using this Router concept to build up our application:
    let app = Router::new()
        .route("/", get(home))
        .layer(AddExtensionLayer::new(AppState::default()));
    // This says, essentially, "please call the home function when you receive a request for /, and
    // add a middleware that does that whole extension thing."

    // Anyway, our app value is now a Router. But a Router cannot be directly run by Hyper.
    // Instead, we need to convert it into a MakeService (a.k.a. an app factory).
    // Fortunately, that's easy: we call app.into_make_service().
    let server = Server::bind(&addr).serve(app.into_make_service());

    if let Err(e) = server.await {
        eprintln!("server error: {e}");
    }
}

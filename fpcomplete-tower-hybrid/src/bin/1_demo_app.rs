use std::{
    future::Future,
    pin::Pin,
    sync::{atomic::AtomicUsize, Arc},
    task::Poll,
};

use fpcomplete_tower_hybrid::{fakeserver, http};

#[derive(Default)]
pub struct DemoApp {
    // give it an atomic counter to make things slightly interesting
    counter: Arc<AtomicUsize>,
}

impl tower::Service<http::Request> for DemoApp {
    type Response = crate::http::Response;
    type Error = anyhow::Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>> + Send>>;

    fn poll_ready(
        &mut self,
        _cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Self::Error>> {
        // always ready for a request
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, mut req: http::Request) -> Self::Future {
        // We're going to implement some logic to increment the counter, fail 25% of the time,
        // and otherwise echo back the request from the user, with an added X-Counter response header.
        let counter = self.counter.clone();
        Box::pin(async move {
            println!("Handling a request for {}", req.path_and_query);
            let counter = counter.fetch_add(1, std::sync::atomic::Ordering::SeqCst);
            anyhow::ensure!(counter % 4 != 2, "Failing 25% of the time, just for fun");

            req.headers
                .insert("X-Counter".to_owned(), counter.to_string());
            let res = crate::http::Response {
                status: 200,
                headers: req.headers,
                body: req.body,
            };
            Ok::<_, anyhow::Error>(res)
        })
    }
}

#[tokio::main]
async fn main() {
    // run the DemoAppService in our runfakeweb test loop which unfinitely creates fake requests
    fakeserver::run(DemoApp::default()).await;
}

//! Tokio Tower: deep dive
//! see video by David Pedersen, on Youtube: https://www.youtube.com/watch?v=16sU1q8OeeI
//!
#![allow(warnings)]
use futures::future::BoxFuture;
use futures::task::Spawn;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Request, Response, Server};
use pin_project::pin_project;
use std::convert::Infallible;
use std::future::{ready, Future, Ready};
use std::io::{Error, Read};
use std::net::SocketAddr;
use std::pin::Pin;
use std::task::{Context, Poll};
use std::thread::sleep;
use std::time::{Duration, Instant};
use thiserror::Error;
use tokio::time::Sleep;
use tower::{limit, BoxError, Layer, Service, ServiceBuilder};

#[tokio::main]
async fn main() {
    env_logger::init();

    // Construct our SocketAddr to listen on...
    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));

    // And a MakeService to handle each connection...
    // let make_service = make_service_fn(|_conn| async { Ok::<_, Infallible>(service_fn(handle)) });
    let make_service = make_service_fn(|_conn| async {
        // let svc = HelloWorld;
        // let svc = service_fn(handle);
        // let svc = Timeout::new(svc, Duration::from_secs(2));
        // let svc = Logging::new(svc);

        let base_layer = ServiceBuilder::new()
            .layer(LoggingLayer::new())
            .layer(TimeoutLayer::new(Duration::from_secs(2)))
            .into_inner();

        let limit_layer = ServiceBuilder::new()
            .rate_limit(100, Duration::from_secs(1))
            .concurrency_limit(1000)
            .into_inner();

        let svc = ServiceBuilder::new()
            .layer(limit_layer)
            .layer(base_layer)
            .service(service_fn(handle));
        Ok::<_, Infallible>(svc)
    });

    // Then bind and serve...
    let server = Server::bind(&addr).serve(make_service);

    // And run forever...
    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}

struct LoggingLayer;

impl LoggingLayer {
    fn new() -> Self {
        Self {}
    }
}

impl<S> Layer<S> for LoggingLayer {
    type Service = Logging<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Logging::new(inner)
    }
}

struct TimeoutLayer {
    timeout: Duration,
}

impl TimeoutLayer {
    fn new(timeout: Duration) -> Self {
        Self { timeout }
    }
}

impl<S> Layer<S> for TimeoutLayer {
    type Service = Timeout<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Timeout::new(inner, self.timeout)
    }
}

// This is the default handler used by the hyper example, we replace it with a tower service: HelloWorld, below
async fn handle(_req: Request<Body>) -> Result<Response<Body>, Infallible> {
    eprintln!("Hello World got request and starts work");
    tokio::time::sleep(std::time::Duration::from_secs(3)).await;
    eprintln!("Ok, Hello World Response is now ready");
    Ok(Response::new(Body::from("Hello World")))
}

// Service
#[derive(Clone)]
struct HelloWorld;

impl Service<Request<Body>> for HelloWorld {
    type Response = Response<Body>;
    type Error = Infallible;
    type Future = Ready<Result<Self::Response, Self::Error>>;
    // type Future = BoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    // fn call(&mut self, req: Request<Body>) -> Self::Future {
    //     // here we create an allocation on every call, we want to avoid that
    //     Box::pin(async move {
    //         log::debug!("receiving request: {} {}", req.method(), req.uri().path());
    //         let ret = ready(Ok(Response::new(Body::from("Hello World")))).await;
    //         log::debug!("finishing request: {} {}", req.method(), req.uri().path());
    //         ret
    //     })
    // }

    fn call(&mut self, req: Request<Body>) -> Self::Future {
        // a sleep here would block the eventloop, so use the async fn to simulate long running tasks
        ready(Ok(Response::new(Body::from("Hello World"))))
    }
}

// Service Middleware (a service that wraps other services or middlewares)
#[derive(Clone, Copy)]
struct Logging<S> {
    inner: S, // Wrapped inner service
}

impl<S> Logging<S> {
    fn new(inner: S) -> Self {
        Self { inner }
    }
}

impl<S, B> Service<Request<B>> for Logging<S>
where
    S: Service<Request<B>, Response = Response<B>> + Send + 'static, // + Clone
    B: Send + 'static,
    S::Future: Send,
{
    type Response = S::Response;
    type Error = S::Error;
    // type Future = S::Future;
    // type Future = BoxFuture<'static, Result<Self::Response, Self::Error>>;
    type Future = LoggingFuture<S::Future>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx)
    }

    // call using Box Futures, allocates on heap on every call!
    // fn call(&mut self, req: Request<T>) -> Self::Future {
    //     let mut inner = self.inner.clone();
    //     Box::pin(async move {
    //         let reqmethod = req.method().clone();
    //         let requri = req.uri().clone();
    //         log::debug!("receiving request: {reqmethod} {}", requri.path());
    //         let ret = inner.call(req).await;
    //         log::debug!("finishing request: {reqmethod} {}", requri.path());
    //         ret
    //     })
    // }

    fn call(&mut self, req: Request<B>) -> Self::Future {
        let reqmethod = req.method().clone();
        let reqpath = req.uri().path().to_string();
        log::debug!("receiving request: {reqmethod} {reqpath}");
        // start the possibly long time taking operation in our future
        let ret = LoggingFuture {
            future: self.inner.call(req),
            method: reqmethod,
            path: reqpath,
        };
        ret
    }
}

// to prevent the need for a Box::pin allocation each call, we implement our own future
#[pin_project]
struct LoggingFuture<F> {
    #[pin]
    future: F,
    method: hyper::Method,
    path: String,
}

impl<F, B, E> Future for LoggingFuture<F>
where
    F: Future<Output = Result<Response<B>, E>>,
{
    type Output = Result<Response<B>, E>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        // Pin::new(&mut self.future).poll(cx)

        // Log finished as soon as the inner operation returns ready
        let this = self.project();
        if let Poll::Ready(res) = this.future.poll(cx) {
            let status = if let Ok(r) = &res {
                r.status().as_u16()
            } else {
                500
            };
            log::debug!(
                "finishing request: {} {} status: {status}",
                this.method,
                this.path
            );
            Poll::Ready(res)
        } else {
            Poll::Pending
        }
    }
}

#[derive(Clone, Copy)]
struct Timeout<S> {
    inner: S, // Wrapped inner service
    timeout: Duration,
}

impl<S> Timeout<S> {
    fn new(inner: S, timeout: Duration) -> Self {
        Self { inner, timeout }
    }
}

impl<S, R> Service<R> for Timeout<S>
where
    S: Service<R>,
    S::Error: Into<BoxError> + Send + Sync + 'static,
{
    type Response = S::Response;
    // either return the inner error, or the timeout error
    type Error = BoxError;
    type Future = TimeoutFuture<S::Future>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx).map_err(Into::into)
    }

    fn call(&mut self, req: R) -> Self::Future {
        TimeoutFuture {
            future: self.inner.call(req),
            timeout: tokio::time::sleep(self.timeout),
        }
    }
}

#[pin_project]
struct TimeoutFuture<F> {
    #[pin]
    future: F,
    #[pin]
    timeout: Sleep,
}

impl<F, T, E> Future for TimeoutFuture<F>
where
    F: Future<Output = Result<T, E>>,
    E: Into<BoxError> + Send + Sync + 'static,
{
    type Output = Result<T, BoxError>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.project();
        if let Poll::Ready(result) = this.future.poll(cx) {
            match result {
                Err(err) => return Poll::Ready(Err(err.into())),
                Ok(res) => return Poll::Ready(Ok(res)),
            }
        }
        // otherwise the feature is not ready, look at the timeout
        if let Poll::Ready(result) = this.timeout.poll(cx) {
            return Poll::Ready(Err(Box::new(TimeoutError::Elapsed)));
        }

        // if the timeout also did not occur it's pending
        Poll::Pending
    }
}

#[derive(Error, Debug)]
pub enum TimeoutError {
    #[error("timeout elapsed")]
    Elapsed,
}

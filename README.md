# Rust Toybox

Collection of my Rust learnings, tests, patterns, ...

## VSCode Plugins

- Rust for VSCode (Microsoft)
- Better TOML (bungcip)
- rust-analyzer

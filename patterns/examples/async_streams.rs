use std::time::Duration;
use tokio::time::Instant;
// use tokio_stream::{self as stream, StreamExt};
use futures::future;
use futures::stream::{self, StreamExt};
use futures::task::Poll;

// #[tokio::main]
#[tokio::main(flavor = "current_thread")]
async fn main() {
    let events = [
        String::from("first"),
        String::from("second"),
        String::from("third"),
    ];
    let delays = [
        Duration::from_millis(10),
        Duration::from_millis(20),
        Duration::from_millis(30),
    ];

    let stream_events = stream::iter(events);
    let stream_delays = stream::iter(delays);

    let mut i = 0;
    let stop_fut = future::poll_fn(move |_cx| {
        i += 1;
        if i <= 5 {
            println!("{:?} {:?}", Instant::now(), "pending");
            Poll::Pending
        } else {
            Poll::Ready(())
        }
    });

    // -> try-fileout-write -> repeat -> take_while

    tokio::spawn(async move {
        let repeated_events = futures::stream::StreamExt::cycle(stream_events);
        let repeated_delays = futures::stream::StreamExt::cycle(stream_delays);

        let item_stream = futures::stream::StreamExt::zip(repeated_events, repeated_delays);
        // tokio::pin!(item_stream);
        let throttled_stream =
            tokio_stream::StreamExt::throttle(item_stream, Duration::from_secs(1));

        // let interval_stream = futures::stream::repeat(()).throttle(Duration::from_millis(5000));

        // let event_stream =
        //     futures::stream::StreamExt::left_stream(throttled_stream, interval_stream);

        let stop_stream = futures::stream::StreamExt::take_until(throttled_stream, stop_fut);

        let final_stream = stop_stream;
        tokio::pin!(final_stream);

        loop {
            tokio::select! {
              // todo: create stream dynamically
              Some(e) = final_stream.next() => {
                println!("{:?} {:?}", Instant::now(), e);
              },
              else => break
            }
        }
    })
    .await
    .unwrap();
}

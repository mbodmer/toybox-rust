//! https://morioh.com/p/1ef163676c39

use anyhow::Result;
use futures::{future::BoxFuture, Future};
use std::collections::HashMap;

type HandlerArgs = (i32, i32);
type HandlerResult = Result<String>;

pub struct Handler {
    func: Box<dyn Fn(HandlerArgs) -> BoxFuture<'static, HandlerResult> + Send + Sync + 'static>,
}

impl Handler {
    pub fn new<P>(raw_func: fn(a: i32, b: i32) -> P) -> Handler
    where
        P: Future<Output = HandlerResult> + Send + 'static,
    {
        Handler {
            func: Box::new(move |(a, b)| Box::pin(raw_func(a, b))),
        }
    }

    pub async fn call(&self, args: HandlerArgs) -> HandlerResult {
        (self.func)(args).await
    }
}

#[derive(Default)]
pub struct Router {
    handlers: HashMap<String, Handler>,
}

impl Router {
    // Use the builder pattern to add handlers
    pub fn add_handler<P>(mut self, name: &str, fun: fn(i32, i32) -> P) -> Self
    where
        P: Future<Output = HandlerResult> + Send + 'static,
    {
        self.handlers.insert(name.to_string(), Handler::new(fun));
        self
    }

    pub fn get(&self, name: &str) -> Result<&Handler> {
        self.handlers
            .get(name)
            .ok_or_else(|| anyhow::anyhow!("no handler for {name}"))
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let router = Router::default()
        .add_handler("add", add)
        .add_handler("sub", sub)
        .add_handler("mul", |a: i32, b: i32| async move {
            Ok(format!("{a} * {b} = {}", a * b))
        });

    println!("add -> {}", router.get("add")?.call((4, 5)).await?);
    println!("sub -> {}", router.get("sub")?.call((4, 5)).await?);
    println!("mul -> {}", router.get("mul")?.call((4, 5)).await?);
    Ok(())
}

async fn add(a: i32, b: i32) -> Result<String> {
    Ok(format!("{a} + {b} = {}", a + b))
}

async fn sub(a: i32, b: i32) -> Result<String> {
    Ok(format!("{a} - {b} = {}", a - b))
}

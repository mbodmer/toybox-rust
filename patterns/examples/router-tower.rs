//! Standalone test for an async router handling tower services

use std::future::Future;
use std::pin::Pin;
use std::task::{Context, Poll};
use std::{collections::HashMap, convert::Infallible};
use tower::{service_fn, util::BoxService, Service};

struct MyRequest {
    path: String,
}

type MyResponse = String;

struct Router {
    handlers: HashMap<String, BoxService<MyRequest, MyResponse, Infallible>>,
}

impl Router {
    fn new() -> Self {
        Self {
            handlers: HashMap::new(),
        }
    }

    // use builder pattern to add routes and service handlers to the handlers map
    fn route<S>(mut self, route: &str, service: S) -> Self
    where
        S: Service<MyRequest, Error = Infallible, Response = MyResponse> + Send + 'static,
        S::Future: Send,
    {
        let boxed = BoxService::new(service);
        self.handlers.insert(route.to_owned(), boxed);
        self
    }
}

impl Service<MyRequest> for Router {
    type Response = MyResponse;
    type Error = Infallible;
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

    fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: MyRequest) -> Self::Future {
        // Look up the handler for the request's path in the handlers map.
        let handler = self.handlers.get_mut(&req.path).unwrap();
        handler.call(req)
    }
}

async fn handler_one(_req: MyRequest) -> Result<MyResponse, Infallible> {
    Ok("handler_one".to_string())
}

async fn handler_two(_req: MyRequest) -> Result<MyResponse, Infallible> {
    Ok("handler_two".to_string())
}

#[tokio::main]
async fn main() {
    // setup routes and handlers
    let mut router = Router::new()
        .route("one", service_fn(handler_one))
        .route("two", service_fn(handler_two));

    // setup requests and call the router which should in turn call the correct request identified by the path
    let reqone = MyRequest {
        path: "one".to_string(),
    };
    if let Ok(r) = router.call(reqone).await {
        println!("{r}");
    }

    let reqtwo = MyRequest {
        path: "two".to_string(),
    };
    if let Ok(r) = router.call(reqtwo).await {
        println!("{r}");
    }
}

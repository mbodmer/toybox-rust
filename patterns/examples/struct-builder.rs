#[derive(Debug)]
struct User {
    email: Option<String>,
    first_name: Option<String>,
    last_name: Option<String>,
}

impl User {
    fn builder() -> UserBuilder {
        UserBuilder::new()
    }
}

struct UserBuilder {
    email: Option<String>,
    first_name: Option<String>,
    last_name: Option<String>,
}

impl UserBuilder {
    fn new() -> Self {
        Self {
            email: None,
            first_name: None,
            last_name: None,
        }
    }

    fn email(mut self, email: impl Into<String>) -> Self {
        self.email = Some(email.into());
        self
    }

    fn first_name(mut self, first_name: impl Into<String>) -> Self {
        self.first_name = Some(first_name.into());
        self
    }

    fn last_name(mut self, last_name: impl Into<String>) -> Self {
        self.last_name = Some(last_name.into());
        self
    }

    fn build(self) -> User {
        let Self {
            email,
            first_name,
            last_name,
        } = self;
        User {
            email,
            first_name,
            last_name,
        }
    }
}

fn main() {
    let user_man = User {
        email: Some("user@example.com".to_string()),
        first_name: Some("First".to_string()),
        last_name: Some("Last".to_string()),
    };

    println!(
        "User created manually: {} {} <{}>",
        user_man.first_name.unwrap(),
        user_man.last_name.unwrap(),
        user_man.email.unwrap(),
    );

    let user_bldr = User::builder()
        .email("user2@email.com")
        .first_name("The")
        .last_name("User")
        .build();

    println!(
        "User created by builder: {} {} <{}>",
        user_bldr.first_name.unwrap(),
        user_bldr.last_name.unwrap(),
        user_bldr.email.unwrap(),
    );
}

// Enum Definition, zusätzlich sind je nach Enum Wert auch
// weitere Felder möglich, welche unterschiedliche Datentypen
// haben können. Hier z.B. ein Result Enum mit den Werten OK
// und Err, bei Ok kann dem Wert ein String angehängt werden,
// im Err Fall wird aber ein Int für den Fehlercode erwartet.
// In der Standard Library gibt es genau so einen Enum Result,
// beide Typen sind aber Generics, also wählbar.
enum MyResult {
    Ok(String),
    Err(u32),
}

fn check_result(r: MyResult) {
    match r {
        MyResult::Ok(msg) => println!("OK: {}", msg),
        MyResult::Err(code) => println!("Err: {}", code),
    }
}

fn main() {
    let result_ok = MyResult::Ok("hallo, alles OK".to_string());
    check_result(result_ok);
    let result_err = MyResult::Err(321);
    check_result(result_err);

    // Ausgabe:
    // OK: hallo, alles OK
    // Err: 321
}

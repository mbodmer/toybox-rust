use std::convert::TryFrom;
use thiserror::Error;

// Protobuf encoding: https://protobuf.dev/programming-guides/encoding/

#[derive(Debug, Error)]
enum Error {
    #[error("Invalid varint")]
    InvalidVarint,
    #[error("Invalid wire-type")]
    InvalidWireType,
    #[error("Unexpected EOF")]
    UnexpectedEOF,
    #[error("Invalid length")]
    InvalidSize(#[from] std::num::TryFromIntError),
    #[error("Unexpected wire-type)")]
    UnexpectedWireType,
    #[error("Invalid string (not UTF-8)")]
    InvalidString,
}

/// A wire type as seen on the wire.
#[derive(Debug, PartialEq)]
enum WireType {
    /// Variable-width integers, or varints, are at the core of the wire format.
    /// They allow encoding unsigned 64-bit integers using anywhere between one
    /// and ten bytes, with small values using fewer bytes.
    /// The Varint WireType indicates the value is a single VARINT.
    Varint,
    //I64,  -- not needed for this exercise
    /// The Len WireType indicates that the value is a length represented as a
    /// VARINT followed by exactly that number of bytes.
    Len,
    /// The I32 WireType indicates that the value is precisely 4 bytes in
    /// little-endian order containing a 32-bit signed integer.
    I32,
}

#[derive(Debug, PartialEq)]
/// A field's value, typed based on the wire type.
enum FieldValue<'a> {
    Varint(u64),
    //I64(i64),  -- not needed for this exercise
    Len(&'a [u8]),
    I32(i32),
}

#[derive(Debug, PartialEq)]
/// A field, containing the field number and its value.
struct Field<'a> {
    field_num: u64,
    value: FieldValue<'a>,
}

trait ProtoMessage<'a>: Default + 'a {
    fn add_field(&mut self, field: Field<'a>) -> Result<(), Error>;
}

impl TryFrom<u64> for WireType {
    type Error = Error;

    fn try_from(value: u64) -> Result<WireType, Error> {
        Ok(match value {
            0 => WireType::Varint,
            //1 => WireType::I64,  -- not needed for this exercise
            2 => WireType::Len,
            5 => WireType::I32,
            _ => return Err(Error::InvalidWireType),
        })
    }
}

impl<'a> FieldValue<'a> {
    fn as_string(&self) -> Result<&'a str, Error> {
        let FieldValue::Len(data) = self else {
            return Err(Error::UnexpectedWireType);
        };
        std::str::from_utf8(data).map_err(|_| Error::InvalidString)
    }

    fn as_bytes(&self) -> Result<&'a [u8], Error> {
        let FieldValue::Len(data) = self else {
            return Err(Error::UnexpectedWireType);
        };
        Ok(data)
    }

    fn as_u64(&self) -> Result<u64, Error> {
        let FieldValue::Varint(value) = self else {
            return Err(Error::UnexpectedWireType);
        };
        Ok(*value)
    }
}

/// Parse a VARINT, returning the parsed value and the remaining bytes.
fn parse_varint(data: &[u8]) -> Result<(u64, &[u8]), Error> {
    // parse max 8 bytes, if not returned earlier
    for i in 0..7 {
        let Some(b) = data.get(i) else {
            return Err(Error::InvalidVarint);
        };
        // Each byte in the varint has a continuation bit that indicates
        // if the byte that follows it is part of the varint. This is the
        // most significant bit (MSB) of the byte (sometimes also called
        // the sign bit). The lower 7 bits are a payload; the resulting
        // integer is built by appending together the 7-bit payloads of
        // its constituent bytes.
        // -> check if the last bit of the current byte is set, if it is
        //    not set, return the accumulated value
        if b & 0x80 == 0 {
            // This is the last byte of the VARINT, so convert it to a u64 and return it.
            let mut value = 0u64;
            // gather the data from current point in for loop (i) back to the beginning.
            // convert (cast) and return it
            for b in data[..=i].iter().rev() {
                value = (value << 7) | (b & 0x7f) as u64;
            }
            return Ok((value, &data[i + 1..]));
        }
    }

    // More than 7 bytes is invalid.
    Err(Error::InvalidVarint)
}

/// Convert a tag into a field number and a WireType.
fn unpack_tag(tag: u64) -> Result<(u64, WireType), Error> {
    let field_num = tag >> 3;
    let wire_type = WireType::try_from(tag & 0x7)?;
    Ok((field_num, wire_type))
}

/// Parse a field, returning the remaining bytes
fn parse_field(data: &[u8]) -> Result<(Field, &[u8]), Error> {
    // first varint is the tag
    let (tag, remainder) = parse_varint(data)?;
    let (field_num, wire_type) = unpack_tag(tag)?;
    let (fieldvalue, remainder) = match wire_type {
        WireType::Varint => {
            let (val, rem) = parse_varint(remainder)?;
            (FieldValue::Varint(val), rem)
        }
        WireType::Len => {
            // when we get Len, we expect a varint again, parse it from the remaining data
            let (len, remainder) = parse_varint(remainder)?;
            let len: usize = len.try_into()?;
            if remainder.len() < len {
                return Err(Error::UnexpectedEOF);
            }
            let (value, remainder) = remainder.split_at(len);
            (FieldValue::Len(value), remainder)
        }
        WireType::I32 => {
            if remainder.len() < 4 {
                return Err(Error::UnexpectedEOF);
            }
            let (value, remainder) = remainder.split_at(4);
            // Unwrap error because `value` is definitely 4 bytes long.
            let value = i32::from_le_bytes(value.try_into().unwrap());
            (FieldValue::I32(value), remainder)
        }
    };
    let field = Field {
        field_num,
        value: fieldvalue,
    };
    Ok((field, remainder))
}

/// Parse a message in the given data, calling `T::add_field` for each field in
/// the message.
///
/// The entire input is consumed.
fn parse_message<'a, T: ProtoMessage<'a>>(mut data: &'a [u8]) -> Result<T, Error> {
    // create default version of target object
    let mut result = T::default();
    //
    while !data.is_empty() {
        let parsed_field = parse_field(data)?;
        result.add_field(parsed_field.0)?;
        data = parsed_field.1;
    }
    Ok(result)
}

#[derive(Debug, Default, PartialEq)]
struct PhoneNumber<'a> {
    number: &'a str,
    type_: &'a str,
}

impl<'a> ProtoMessage<'a> for PhoneNumber<'a> {
    fn add_field(&mut self, field: Field<'a>) -> Result<(), Error> {
        match field.field_num {
            1 => self.number = field.value.as_string()?,
            2 => self.type_ = field.value.as_string()?,
            _ => {}
        }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq)]
struct Person<'a> {
    name: &'a str,
    id: u64,
    phone: Vec<PhoneNumber<'a>>,
}

impl<'a> ProtoMessage<'a> for Person<'a> {
    fn add_field(&mut self, field: Field<'a>) -> Result<(), Error> {
        match field.field_num {
            1 => self.name = field.value.as_string()?,
            2 => self.id = field.value.as_u64()?,
            3 => self.phone.push(
                // here we parse the contents again as PhoneNumber
                parse_message(field.value.as_bytes()?)?,
            ),
            _ => {}
        }
        Ok(())
    }
}

fn main() {
    let person: Person = parse_message(&[
        0x0a, 0x07, 0x6d, 0x61, 0x78, 0x77, 0x65, 0x6c, 0x6c, 0x10, 0x2a, 0x1a, 0x16, 0x0a, 0x0e,
        0x2b, 0x31, 0x32, 0x30, 0x32, 0x2d, 0x35, 0x35, 0x35, 0x2d, 0x31, 0x32, 0x31, 0x32, 0x12,
        0x04, 0x68, 0x6f, 0x6d, 0x65, 0x1a, 0x18, 0x0a, 0x0e, 0x2b, 0x31, 0x38, 0x30, 0x30, 0x2d,
        0x38, 0x36, 0x37, 0x2d, 0x35, 0x33, 0x30, 0x38, 0x12, 0x06, 0x6d, 0x6f, 0x62, 0x69, 0x6c,
        0x65,
    ])
    .unwrap();
    println!("{:#?}", person);
}

#[cfg(test)]
mod test {
    use super::*;

    const MESSAGE: &[u8] = &[
        0x0a, 0x07, 0x6d, 0x61, 0x78, 0x77, 0x65, 0x6c, 0x6c, 0x10, 0x2a, 0x1a, 0x16, 0x0a, 0x0e,
        0x2b, 0x31, 0x32, 0x30, 0x32, 0x2d, 0x35, 0x35, 0x35, 0x2d, 0x31, 0x32, 0x31, 0x32, 0x12,
        0x04, 0x68, 0x6f, 0x6d, 0x65, 0x1a, 0x18, 0x0a, 0x0e, 0x2b, 0x31, 0x38, 0x30, 0x30, 0x2d,
        0x38, 0x36, 0x37, 0x2d, 0x35, 0x33, 0x30, 0x38, 0x12, 0x06, 0x6d, 0x6f, 0x62, 0x69, 0x6c,
        0x65,
    ];

    #[test]
    fn as_string() {
        assert!(FieldValue::Varint(10).as_string().is_err());
        assert!(FieldValue::I32(10).as_string().is_err());
        assert_eq!(FieldValue::Len(b"hello").as_string().unwrap(), "hello");
    }

    #[test]
    fn as_bytes() {
        assert!(FieldValue::Varint(10).as_bytes().is_err());
        assert!(FieldValue::I32(10).as_bytes().is_err());
        assert_eq!(FieldValue::Len(b"hello").as_bytes().unwrap(), b"hello");
    }

    #[test]
    fn as_u64() {
        assert_eq!(FieldValue::Varint(10).as_u64().unwrap(), 10u64);
        assert!(FieldValue::I32(10).as_u64().is_err());
        assert!(FieldValue::Len(b"hello").as_u64().is_err());
    }

    #[test]
    fn parse_varint() {
        // first we expect a varint in the message (a field tag), parse it
        let (tag, remainder) = super::parse_varint(&MESSAGE[..]).unwrap();
        // the first byte is 10, with its MSB=0, so its directly the varint value (tag) we expect
        assert_eq!(tag, 0x0a);
        // remainder is messge minus the first byte
        assert_eq!(remainder, &MESSAGE[1..]);
    }

    #[test]
    fn unpack_tag() {
        // lets parse our first tag into the field number and a wiretype
        let (field_num, wire_type) = super::unpack_tag(0x0a).unwrap();
        assert_eq!(field_num, 1);
        assert_eq!(wire_type, super::WireType::Len);
    }

    #[test]
    fn parse_first_2_fields() {
        // the first field is a len fieldtype containing the name
        let (a, remainder) = super::parse_field(&MESSAGE[..]).unwrap();
        assert_eq!(
            Field {
                field_num: 1,
                value: FieldValue::Len(&MESSAGE[2..9])
            },
            a
        );
        assert_eq!(remainder, &MESSAGE[9..]);
        assert_eq!(a.value.as_string().unwrap(), "maxwell");
        // the second field is a varint fieldtype containing the id
        let (b, remainder2) = super::parse_field(remainder).unwrap();
        assert_eq!(
            Field {
                field_num: 2,
                value: FieldValue::Varint(42) // byte 10
            },
            b
        );
        assert_eq!(b.value.as_u64().unwrap(), 42);
        assert_eq!(remainder2, &MESSAGE[11..]);
        // the third field is a len fieldtype containing the home number
        let (c, remainder3) = super::parse_field(remainder2).unwrap();
        assert_eq!(
            Field {
                field_num: 3,
                value: FieldValue::Len(&MESSAGE[13..35])
            },
            c
        );
        assert_eq!(remainder3, &MESSAGE[35..]);
        assert_eq!(
            c.value.as_string().unwrap(),
            "\n\u{e}+1202-555-1212\u{12}\u{4}home"
        );
        // the fourth field is a len fieldtype containing the mobile number
        let (d, remainder4) = super::parse_field(remainder3).unwrap();
        assert_eq!(
            Field {
                field_num: 3,
                value: FieldValue::Len(&MESSAGE[37..61])
            },
            d
        );
        assert_eq!(remainder4, &[]);
        assert_eq!(
            d.value.as_string().unwrap(),
            "\n\u{e}+1800-867-5308\u{12}\u{6}mobile"
        );
    }

    #[test]
    fn parse_phone_number() {
        let number1: PhoneNumber = super::parse_message(&MESSAGE[13..]).unwrap();
        assert_eq!(
            PhoneNumber {
                number: "+1202-555-1212",
                type_: "home"
            },
            number1
        );
        let number2: PhoneNumber = super::parse_message(&MESSAGE[37..]).unwrap();
        assert_eq!(
            PhoneNumber {
                number: "+1800-867-5308",
                type_: "mobile"
            },
            number2
        );
    }

    #[test]
    fn parse_message() {
        let person: Person = super::parse_message(&MESSAGE).unwrap();
        assert_eq!(
            Person {
                name: "maxwell",
                id: 42,
                phone: vec![
                    PhoneNumber {
                        number: "+1202-555-1212",
                        type_: "home",
                    },
                    PhoneNumber {
                        number: "+1800-867-5308",
                        type_: "mobile",
                    },
                ],
            },
            person
        );
    }
}

use std::future::Future;
use std::pin::Pin;
use std::task::{Context, Poll};
use tokio::time::{Duration, Sleep};
use tokio_stream::Stream;

/// Async Iterator (`Stream`) repeatedly yields elements of a sequence with specified delays
pub struct DelaySeq {
    /// bookkeeping the repetitions
    rep_counter: usize,
    /// the currently active delay, 0 when inactive
    active_delay: Pin<Box<Sleep>>,
    /// the sequence to stream, with delay for each element
    sequence: Vec<FileoutPattern>,
    /// pointer to the current position in the sequence
    sequence_pos: usize,
}

impl DelaySeq {
    /// # Parameters
    /// - `sequence` - a slice of `FileoutPattern` (String, Duration) tuples
    /// - `times` - how many times the sequence should be repeated before the stream gets ready
    #[allow(unused)]
    #[must_use]
    pub fn new(sequence: &[FileoutPattern], times: usize) -> DelaySeq {
        Self {
            rep_counter: times,
            active_delay: Box::pin(tokio::time::sleep(sequence[0].1)),
            sequence: sequence.to_vec(),
            sequence_pos: 0,
        }
    }
}

impl Default for DelaySeq {
    fn default() -> Self {
        Self {
            rep_counter: 0,
            active_delay: Box::pin(tokio::time::sleep(Duration::ZERO)),
            sequence: Vec::new(),
            sequence_pos: 0,
        }
    }
}

impl Stream for DelaySeq {
    type Item = String;
    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<String>> {
        if self.rep_counter == 0 {
            return Poll::Ready(None);
        }

        let pos = self.sequence_pos;
        if self.active_delay.as_mut().poll(cx).is_ready() {
            let delay_duration = self.sequence[pos].1;
            self.active_delay.set(tokio::time::sleep(delay_duration));

            if self.sequence_pos < self.sequence.len() - 1 {
                self.sequence_pos += 1;
            } else {
                self.rep_counter -= 1;
                self.sequence_pos = 0;
            }
            Poll::Ready(Some(self.sequence[pos].0.clone()))
        } else {
            Poll::Pending
        }
    }
}

#[cfg(test)]
mod tests {
    use itertools::Itertools;
    use tokio::time::Duration;
    use tokio_stream::StreamExt;

    use crate::utils::settings::FileoutPattern;

    use super::DelaySeq;

    #[tokio::test]
    async fn test_delayseq() {
        let seq = [
            FileoutPattern(String::from("first"), Duration::from_millis(10)),
            FileoutPattern(String::from("second"), Duration::from_millis(10)),
            FileoutPattern(String::from("third"), Duration::from_millis(10)),
        ];
        // imitate default for select! statement
        let mut ds = DelaySeq::default();
        assert_eq!(None, ds.next().await);
        // overwrite ds when a new deleay sequence should run
        ds = DelaySeq::new(&seq[..], 3);

        let mut results = Vec::new();
        while let Some(v) = &ds.next().await {
            results.push(v.clone());
        }
        let expected = seq
            .iter()
            .map(|e| e.0.to_string())
            .cycle()
            .take(3 * seq.len())
            .collect_vec();
        assert_eq!(expected, results);
    }
}

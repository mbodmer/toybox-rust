use std::future::Future;
use std::pin::Pin;
use std::sync::Arc;
use std::sync::Mutex;
use std::task::{Context, Poll};
use tokio::time::{Duration, Sleep};
use tokio_stream::Stream;

/// Async Iterator (`Stream`) repeatedly calls a closure while it returns true until it returns false
///
/// This stream can be used to drive/transition an object into it's desired state with multiple
/// iterations until the state is reached
pub struct Transition {
    // the duration between iterations
    period: Duration,
    // the current iteration count, this is to prevent infinite loops
    counter: usize,
    // the currently active sleep
    delay: Pin<Box<Sleep>>,
    // the closure to call on each iteration
    closure: Arc<Mutex<dyn FnMut() -> bool>>,
}

impl Default for Transition {
    fn default() -> Self {
        Self::new(Duration::ZERO, 0, Arc::new(Mutex::from(|| false)))
    }
}

impl Transition {
    pub fn new(
        period: Duration,
        maxiter: usize,
        // closure: impl FnMut() -> bool + 'static + Send,
        closure: Arc<Mutex<dyn FnMut() -> bool>>,
    ) -> Self {
        Self {
            period,
            counter: maxiter,
            delay: Box::pin(tokio::time::sleep(period)),
            closure,
        }
    }
}

impl Stream for Transition {
    type Item = ();
    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<()>> {
        if self.counter == 0 {
            return Poll::Ready(None);
        }

        let period = self.period;
        if self.delay.as_mut().poll(cx).is_ready() {
            self.delay.set(tokio::time::sleep(period));
            self.counter -= 1;
            if (self.closure.lock().unwrap())() {
                Poll::Ready(Some(()))
            } else {
                Poll::Ready(None)
            }
        } else {
            Poll::Pending
        }
    }
}

#[cfg(test)]
mod tests {

    /// Object to be called `counter` times, returns false, if ready
    struct TestHandler {
        counter: usize,
    }
    impl TestHandler {
        fn countdown(&mut self) -> bool {
            if self.counter == 0 {
                return false;
            }
            self.counter -= 1;
            true
        }
    }

    use std::{cell::RefCell, rc::Rc};

    use tokio::time::Duration;
    use tokio_stream::StreamExt;

    use super::Transition;

    #[tokio::test]
    async fn test_transition() {
        let h = Rc::new(RefCell::new(TestHandler { counter: 3 }));

        // imitate default for select! statement
        // let mut t = Transition::default();
        let mut t = Box::pin(Transition::default());
        // should always be Ready(None)
        assert_eq!(None, t.next().await);

        // // overwrite ds when a new transition should run
        // let h_closure = Rc::clone(&h);
        // t.set(Transition::new(Duration::from_millis(10), 5, move || {
        //     h_closure.borrow_mut().countdown()
        // }));

        let mut counter = 0;
        while t.next().await.is_some() {
            counter += 1;
        }
        assert_eq!(3, counter);
    }
}
